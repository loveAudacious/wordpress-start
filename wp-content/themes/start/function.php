<?php 

function get_the_post_thumbnail( $post = null, $size = 'post-thumbnail', $attr = '' ) {
    $post = get_post( $post );
    if ( ! $post ) {
        return '';
    }
    $post_thumbnail_id = get_post_thumbnail_id( $post );
 
    /**
     * Filters the post thumbnail size.
     *
     * @since 2.9.0
     * @since 4.9.0 Added the `$post_id` parameter.
     *
     * @param string|array $size    The post thumbnail size. Image size or array of width and height
     *                              values (in that order). Default 'post-thumbnail'.
     * @param int          $post_id The post ID.
     */
    $size = apply_filters( 'post_thumbnail_size', $size, $post->ID );
 
    if ( $post_thumbnail_id ) {
 
        /**
         * Fires before fetching the post thumbnail HTML.
         *
         * Provides "just in time" filtering of all filters in wp_get_attachment_image().
         *
         * @since 2.9.0
         *
         * @param int          $post_id           The post ID.
         * @param string       $post_thumbnail_id The post thumbnail ID.
         * @param string|array $size              The post thumbnail size. Image size or array of width
         *                                        and height values (in that order). Default 'post-thumbnail'.
         */
        do_action( 'begin_fetch_post_thumbnail_html', $post->ID, $post_thumbnail_id, $size );
        if ( in_the_loop() )
            update_post_thumbnail_cache();
        $html = wp_get_attachment_image( $post_thumbnail_id, $size, false, $attr );
 
        /**
         * Fires after fetching the post thumbnail HTML.
         *
         * @since 2.9.0
         *
         * @param int          $post_id           The post ID.
         * @param string       $post_thumbnail_id The post thumbnail ID.
         * @param string|array $size              The post thumbnail size. Image size or array of width
         *                                        and height values (in that order). Default 'post-thumbnail'.
         */
        do_action( 'end_fetch_post_thumbnail_html', $post->ID, $post_thumbnail_id, $size );
 
    } else {
        $html = '';
    }
    /**
     * Filters the post thumbnail HTML.
     *
     * @since 2.9.0
     *
     * @param string       $html              The post thumbnail HTML.
     * @param int          $post_id           The post ID.
     * @param string       $post_thumbnail_id The post thumbnail ID.
     * @param string|array $size              The post thumbnail size. Image size or array of width and height
     *                                        values (in that order). Default 'post-thumbnail'.
     * @param string       $attr              Query string of attributes.
     */
    return apply_filters( 'post_thumbnail_html', $html, $post->ID, $post_thumbnail_id, $size, $attr );
}

function get_the_post_thumbnail_url( $post = null, $size = 'post-thumbnail' ) {
    $post_thumbnail_id = get_post_thumbnail_id( $post );
    if ( ! $post_thumbnail_id ) {
        return false;
    }
    return wp_get_attachment_image_url( $post_thumbnail_id, $size );
}

function wpb_lastupdated_posts() { 

    

 
    // Query Arguments
    $lastupdated_args = array(
    'orderby' => 'modified',
    'ignore_sticky_posts' => '1'
    );
     
    //Loop to display 5 recently updated posts
    $lastupdated_loop = new WP_Query( $lastupdated_args );
    $counter = 1;
    $string .= '<ul>';
    while( $lastupdated_loop->have_posts() && $counter < 5 ) : $lastupdated_loop->the_post();
    $string .= '<li><a href="' . get_permalink( $lastupdated_loop->post->ID ) . '"> ' .get_the_title( $lastupdated_loop->post->ID ) . '</a> ( '. get_the_modified_date() .') </li>';
    $counter++;
    endwhile; 
    $string .= '</ul>';
    return $string;
    wp_reset_postdata(); 
    } 
     
    //add a shortcode
    add_shortcode('lastupdated-posts', 'wpb_lastupdated_posts');

function get_home_url( $blog_id = null, $path = '', $scheme = null ) {
    global $pagenow;
 
    $orig_scheme = $scheme;
 
    if ( empty( $blog_id ) || !is_multisite() ) {
        $url = get_option( 'home' );
    } else {
        switch_to_blog( $blog_id );
        $url = get_option( 'home' );
        restore_current_blog();
    }
 
    if ( ! in_array( $scheme, array( 'http', 'https', 'relative' ) ) ) {
        if ( is_ssl() && ! is_admin() && 'wp-login.php' !== $pagenow )
            $scheme = 'https';
        else
            $scheme = parse_url( $url, PHP_URL_SCHEME );
    }
 
    $url = set_url_scheme( $url, $scheme );
 
    if ( $path && is_string( $path ) )
        $url .= '/' . ltrim( $path, '/' );
 
    /**
     * Filters the home URL.
     *
     * @since 3.0.0
     *
     * @param string      $url         The complete home URL including scheme and path.
     * @param string      $path        Path relative to the home URL. Blank string if no path is specified.
     * @param string|null $orig_scheme Scheme to give the home URL context. Accepts 'http', 'https',
     *                                 'relative', 'rest', or null.
     * @param int|null    $blog_id     Site ID, or null for the current site.
     */
    return apply_filters( 'home_url', $url, $path, $orig_scheme, $blog_id );
}

?>