<!DOCTYPE html>
	<html lang="zxx" class="no-js">
	<head>
		<!-- Mobile Specific Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!-- Favicon-->
		<link rel="shortcut icon" href="img/fav.png">
		<!-- Author Meta -->
		<meta name="author" content="CodePixar">
		<!-- Meta Description -->
		<meta name="description" content="">
		<!-- Meta Keyword -->
		<meta name="keywords" content="">
		<!-- meta character set -->
		<meta charset="UTF-8">
		<!-- Site Title -->
		<title>Love, Audacious</title>

		<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet"> 
		<link href="https://fonts.googleapis.com/css?family=Lusitana" rel="stylesheet">

			<!--
			CSS
            ============================================= -->
            <link href="<?php echo get_bloginfo('template_directory'); ?>/css/linearicons.css" rel="stylesheet">
            <link href="<?php echo get_bloginfo('template_directory'); ?>/css/font-awesome.min.css" rel="stylesheet">
            <link href="<?php echo get_bloginfo('template_directory'); ?>/css/jquery.DonutWidget.min.css" rel="stylesheet">
            <link href="<?php echo get_bloginfo('template_directory'); ?>/css/bootstrap.css" rel="stylesheet">
            <link href="<?php echo get_bloginfo('template_directory'); ?>/css/owl.carousel.css" rel="stylesheet">
            <link href="<?php echo get_bloginfo('template_directory'); ?>/css/main.css" rel="stylesheet">

            <?php wp_head();?>
		</head>
		<body>

			<!-- Start Header Area -->
			<header class="default-header">
				<nav class="navbar navbar-expand-lg  navbar-light">
					<div class="container">
						  <a class="navbar-brand" href="index.html">
						  	<img src="img/logo.png" alt="">
						  </a>
						  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
						    <span class="navbar-toggler-icon"></span>
						  </button>

						  <div class="collapse navbar-collapse justify-content-end align-items-center" id="navbarSupportedContent">
						    <ul class="navbar-nav">
								<li><a href="<?php echo home_url(); ?>">Home</a></li>
								<!-- <li><a href="#service">Product</a></li> -->
								<li><a href="/audacious/blog">Blog</a></li>
								<li><a href="#team">team</a></li>
							   <!-- Dropdown -->
							    <!-- <li class="dropdown">
							      <a class="dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
							        Pages
							      </a>
							      <div class="dropdown-menu">
							        <a class="dropdown-item" href="generic.html">Generic</a>
							        <a class="dropdown-item" href="elements.html">Elements</a>
							      </div>
							    </li>									 -->
						    </ul>
						  </div>						
					</div>
				</nav>
			</header>
			<!-- End Header Area -->