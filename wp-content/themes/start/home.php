<section class="service-area pt-100 pb-150" id="service">
	<div class="container">
    <div class="row d-flex justify-content-center">
        <div class="menu-content pb-70 col-lg-8">
            <div class="title text-center">
            <h1 class="mb-10">Latest articles to keeping Audacious</h1>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore  et dolore magna aliqua.</p>
            </div>
        </div>
    </div>
    
    <?php get_header(); ?>
    <?php 
    if ( have_posts() ) : while ( have_posts() ) : the_post();
    get_template_part( 'content', get_post_format() );
    endwhile; endif;
    ?>																		
	</div>	
</section>
<?php get_footer(); ?>	
