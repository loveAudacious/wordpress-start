<?php get_header(); ?>

            <!-- start banner Area -->
			<section class="banner-area relative" id="home" data-parallax="scroll" data-image-src="<?php echo get_bloginfo('template_directory'); ?>/img/header-bg.jpg">
				<div class="overlay-bg overlay"></div>
				<div class="container">
					<div class="row fullscreen  d-flex align-items-center justify-content-end">
						<div class="banner-content col-lg-6 col-md-12">
							<h1 style="margin-bottom: 10px; font-size: 67px;">
                            <?php echo get_bloginfo( 'name' ); ?>
								<!-- We Provide  <br>
								<span>Solutions</span> that <br>
								Brings <span>Joy</span>							 -->
							</h1>
							<h3 style="color: dark-gray; margin-bottom: 20px; font-weight:500">One survey. One report. Hours of time saved.</h1><br>
							<a href="#" class="primary-btn2 header-btn text-uppercase" ><?php echo get_bloginfo( 'description' ); ?></a>
						</div>												
					</div>
				</div>
			</section>
			<!-- End banner Area -->	

			
			<!-- start service Area-->
			<section class="service-area pt-100 pb-150" id="service">
				<div class="container">
					<div class="row d-flex justify-content-center">
						<div class="menu-content pb-70 col-lg-8" style="padding-bottom:33px;">
							<div class="title text-center">
								<h1 class="mb-10">Get Your Wedding Plan Up In 5 Minutes</h1>
								<p>Answer simple questions, view your automated wedding report, and contact your favourite vendors!</p>
							</div>
						</div>
					</div>	
					<div class="row">
						<div class="sigle-service col-lg-4 col-md-6 box-padding" style="text-align:center;">
							<span class="lnr lnr-select"></span>
							<h4>Hassle-Free Planning</h4>
							<p>
							Most newlyweds aren't aware of what they don't know in the full wedding planning process. Our survey will guide you through the necessary steps, prompt you to decide on key factors, and consolidate it all in a shareable moodboard.
							</p>
							<!-- <a href="#" class="text-uppercase primary-btn2 primary-border circle">View Details</a> -->
						</div>
						<div class="sigle-service col-lg-4 col-md-6 box-padding" style="text-align:center;">
							<span class="lnr lnr-magic-wand"></span>
							<h4>End-to-end Platform</h4>
							<p>
							Weddings aren't all one and the same. Our goal is to cater to your personalized preferences by smart-matching you to wedding vendors you'll come to know and love.
							</p>
							<!-- <a href="#" class="text-uppercase primary-btn2 primary-border circle">View Details</a> -->
						</div>
						<div class="sigle-service col-lg-4 col-md-6 box-padding" style="text-align:center;">
							<span class="lnr lnr-heart"></span>
							<h4>User-Friendly Process</h4>
							<p>
							Complete a short survey, view your automated wedding moodboard, find relevant vendors. Everything happens at a click of a button!
							</p>
							<!-- <a href="#" class="text-uppercase primary-btn2 primary-border circle">View Details</a> -->
						</div>
						<!-- <div class="sigle-service col-lg-3 col-md-6">
							<span class="lnr lnr-phone"></span>
							<h4>Dedicated Support</h4>
							<p>
								inappropriate behavior is often laughed off as “boys will be boys,” women face higher conduct.
							</p>
							<a href="#" class="text-uppercase primary-btn2 primary-border circle">View Details</a>
						</div>																		 -->

					</div>
				</div>	
			</section>
			<!-- end service Area-->

			<!-- start team Area-->
			<section class="service-area pt-100" id="service" style="background-color: #f7dad4; padding-bottom:100px;">
				<div class="container">
					<div class="row d-flex justify-content-center">
						<div class="menu-content pb-70 col-lg-8" style="padding-bottom:33px;">
							<div class="title text-center">
								<h1 class="mb-30">Your Big Day, Your Audacious Step</h1>
								<p>Our goal is to empower couples to take charge of their wedding planning process, and make it a day to remember. <br>
								We believe that the the start of your wedding journey should be an enjoyable, stress-free one. <br><br>
								And we’re committed to assisting you with all the nitty-gritty details along the way.</p>
							</div>
						</div>
					</div>	
				</div>	
			</section>
			<!-- end team Area-->

			<!-- part 2 -->
			<!-- start moodboard to life-->
			<section class="service-area pt-100 pb-150 section-bkground test" id="service") style="opacity: 0.7;filter: alpha(opacity=70)">
				<div class="container" >
					<div class="row d-flex justify-content-center">
						<div class="menu-content pb-70 col-lg-8">
							<div class="title text-center">
								<h1 class="mb-10">Bring Your Wedding Moodboard To Life</h1>
								<!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore  et dolore magna aliqua.</p> -->
							</div>
						</div>
					</div>	
					<div class="row">
						<div class="sigle-service col-lg-4 col-md-5">
							<center>
							<h3 class="circle">1</h3>
							<h4>Gather Preferences</h4>
							<p>Fill in a quick survey.</p>
						</center>
							<!-- <a href="#" class="text-uppercase primary-btn2 primary-border circle">View Details</a> -->
						</div>
						<div class="sigle-service col-lg-4 col-md-5">
						<center>
							<h3 class="circle">2</h3>
							<h4>Visualize Your Wedding Plan</h4>
							<p>View your wedding moodboard <br>
							(with your preferences organized to a tee!). <br><br>
							Refer to it for your own use, or share it with <br> your wedding planner.</p>
</center>
							<!-- <a href="#" class="text-uppercase primary-btn2 primary-border circle">View Details</a> -->
						</div>
						<div class="sigle-service col-lg-4 col-md-5">
						<center>
							<h3 class="circle">3</h3>
							<h4>Get Matched!</h4>
							<p>To vendors based on your preferences</p>
</center>
							<!-- <a href="#" class="text-uppercase primary-btn2 primary-border circle">View Details</a> -->
						</div>
						<!-- <div class="sigle-service col-lg-3 col-md-6">
							<h3>4</h3>
							<h4>Connect with vendors</h4>
							<p>Using our email templates</p>
							<a href="#" class="text-uppercase primary-btn2 primary-border circle">View Details</a>
						</div>																		 -->

					</div>
				</div>	
			</section>
			<!-- end service Area-->

			<!-- Start About Area -->
			<!-- <section class="about-area">
				<div class="container-fluid">
					<div class="row justify-content-end align-items-center d-flex no-padding">
						<div class="col-lg-6 about-left mt-70">
							<h1>We can be your digital <br>
							Problems Solution Partner</h1>
							<p>
								inappropriate behavior is often laughed off as “boys will be <br> boys,” women face higher conduct standards especially <br> in the workplace. That’s why it’s crucial that, as women, our <br> behavior on the job is beyond reproach.
							</p>
							<div class="buttons">
								<a href="#" class="about-btn text-uppercase primary-border circle">What we offer</a>
								<a href="#" class="about-btn text-uppercase  primary-border circle">Get a free quote</a>
							</div>
						</div>
						<div class="col-lg-6 about-right">
							<img class="img-fluid" src="img/about.png" alt="">
						</div>
					</div>
				</div>	
			</section> -->
			<!-- End About Area -->


			<!-- Start project Area -->
			<!-- <section class="project-area section-gap" id="project">
				<div class="container">
					<div class="row d-flex justify-content-center">
						<div class="menu-content pb-40 col-lg-8">
							<div class="title text-center">
								<h1 class="mb-10">From your inspiration to your Big Day</h1>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut <br> labore  et dolore magna aliqua.</p>
							</div>
						</div>
					</div>						
					<div class="row">
						<div class="active-works-carousel mt-40">
							<div class="item">
								<img class="img-fluid" src="<?php echo get_bloginfo('template_directory'); ?>/img/project.jpg" alt="">
								<div class="caption text-center mt-20">
									<h6 class="text-uppercase">Vector Illustration</h6>
									<p>LCD screens are uniquely modern in style, and the liquid crystals that make them work have <br> allowed humanity to create slimmer, more portable technology.</p>
								</div>
							</div>
							<div class="item">
								<img class="img-fluid" src="<?php echo get_bloginfo('template_directory'); ?>/img/project.jpg" alt="">
								<div class="caption text-center mt-20">
									<h6 class="text-uppercase">Vector Illustration</h6>
									<p>LCD screens are uniquely modern in style, and the liquid crystals that make them work have <br> allowed humanity to create slimmer, more portable technology.</p>
								</div>
							</div>
							<div class="item">
								<img class="img-fluid" src="<?php echo get_bloginfo('template_directory'); ?>/img/project.jpg" alt="">
								<div class="caption text-center mt-20">
									<h6 class="text-uppercase">Vector Illustration</h6>
									<p>LCD screens are uniquely modern in style, and the liquid crystals that make them work have <br> allowed humanity to create slimmer, more portable technology.</p>
								</div>
							</div>
							<div class="item">
								<img class="img-fluid" src="<?php echo get_bloginfo('template_directory'); ?>/img/project.jpg" alt="">
								<div class="caption text-center mt-20">
									<h6 class="text-uppercase">Vector Illustration</h6>
									<p>LCD screens are uniquely modern in style, and the liquid crystals that make them work have <br> allowed humanity to create slimmer, more portable technology.</p>
								</div>
							</div>
							<div class="item">
								<img class="img-fluid" src="<?php echo get_bloginfo('template_directory'); ?>/img/project.jpg" alt="">
								<div class="caption text-center mt-20">
									<h6 class="text-uppercase">Vector Illustration</h6>
									<p>LCD screens are uniquely modern in style, and the liquid crystals that make them work have <br> allowed humanity to create slimmer, more portable technology.</p>
								</div>
							</div>
						</div>
					</div>
				</div>	
			</section> -->
			<!-- End project Area -->
			
		
			<!-- Start skill Area -->
			<!-- <section class="skill-area section-gap">
				<div class="container">
					<div class="row">
						<div class="col-lg-6 skill-left">
							<h1 class="text-white mb-30">Our Fields of Expertness</h1>
							<p>
								inappropriate behavior is often laughed off as “boys will be <br> boys,” women face higher conduct standards especially in <br> the workplace. That’s why it’s crucial that.
							</p>
						</div>
						<div class="col-lg-6 skill-right">
							<div class="row d-flex justify-content-center">
								<div class="col-lg-4 col-md-4 single-skill">
									<div class="skill1 d-block mx-auto"></div>
									<h4>Wireframing</h4>
								</div>
								<div class="col-lg-4 col-md-4 single-skill">
									<div class="skill2 d-block mx-auto"></div>
									<h4>User Research</h4>
								</div>
								<div class="col-lg-4 col-md-4 single-skill">
									<div class="skill3 d-block mx-auto"></div>
									<h4>User Experience</h4>
								</div>																
							</div>
						</div>
					</div>
				</div>	
			</section> -->
			<!-- End skill Area -->
			
			
			<!-- Start team Area -->
			<!-- <section class="team-area section-gap" id="team">
				<div class="container">
					<div class="row d-flex justify-content-center">
						<div class="menu-content pb-70 col-lg-8">
							<div class="title text-center">
								<h1 class="mb-10">About Creative Agency Team</h1>
								<p>Who are in extremely love with eco friendly system.</p>
							</div>
						</div>
					</div>						
					<div class="row justify-content-center d-flex align-items-center">
						<div class="col-md-3 single-team">
						    <div class="thumb">
						        <img class="img-fluid" src="<?php echo get_bloginfo('template_directory'); ?>/img/t1.jpg" alt="">
						        <div class="align-items-center justify-content-center d-flex">
									<a href="#"><i class="fa fa-facebook"></i></a>
									<a href="#"><i class="fa fa-twitter"></i></a>
									<a href="#"><i class="fa fa-linkedin"></i></a>
						        </div>
						    </div>
						    <div class="meta-text mt-30 text-center">
							    <h4>Ethel Davis</h4>
							    <p>Managing Director (Sales)</p>									    	
						    </div>
						</div>
						<div class="col-md-3 single-team">
						    <div class="thumb">
						        <img class="img-fluid" src="<?php echo get_bloginfo('template_directory'); ?>/img/t2.jpg" alt="">
						        <div class="align-items-center justify-content-center d-flex">
									<a href="#"><i class="fa fa-facebook"></i></a>
									<a href="#"><i class="fa fa-twitter"></i></a>
									<a href="#"><i class="fa fa-linkedin"></i></a>
						        </div>
						    </div>
						    <div class="meta-text mt-30 text-center">
							    <h4>Rodney Cooper</h4>
							    <p>Creative Art Director (Project)</p>			    	
						    </div>
						</div>	
						<div class="col-md-3 single-team">
						    <div class="thumb">
						        <img class="img-fluid" src="<?php echo get_bloginfo('template_directory'); ?>/img/t3.jpg" alt="">
						        <div class="align-items-center justify-content-center d-flex">
									<a href="#"><i class="fa fa-facebook"></i></a>
									<a href="#"><i class="fa fa-twitter"></i></a>
									<a href="#"><i class="fa fa-linkedin"></i></a>
						        </div>
						    </div>
						    <div class="meta-text mt-30 text-center">
							    <h4>Dora Walker</h4>
							    <p>Senior Core Developer</p>			    	
						    </div>
						</div>	
						<div class="col-md-3 single-team">
						    <div class="thumb">
						        <img class="img-fluid" src="<?php echo get_bloginfo('template_directory'); ?>/img/t4.jpg" alt="">
						        <div class="align-items-center justify-content-center d-flex">
									<a href="#"><i class="fa fa-facebook"></i></a>
									<a href="#"><i class="fa fa-twitter"></i></a>
									<a href="#"><i class="fa fa-linkedin"></i></a>
						        </div>
						    </div>
						    <div class="meta-text mt-30 text-center">
							    <h4>Lena Keller</h4>
							    <p>Creative Content Developer</p>			    	
						    </div>
						</div>																									
				
					</div>
				</div>	
			</section> -->
			<!-- End team Area -->


			<!-- Start testimonial Area -->
			<!-- <section class="testimonial-area relative section-gap">
				<div class="overlay overlay-bg"></div>
				<div class="container">
					<div class="row">
						<div class="active-testimonial">
							<div class="single-testimonial item d-flex flex-row">
								<div class="thumb">
									<img class="img-fluid" src="<?php echo get_bloginfo('template_directory'); ?>/img/user1.png" alt="">
								</div>
								<div class="desc">
									<p>
										Accessories Here you can find the best computer accessory for your laptop, monitor, printer, scanner, speaker, projector, hardware.
									</p>
									<h4 mt-30>Mark Alviro Wiens</h4>
									<p>CEO at Google</p>
								</div>
							</div>
							<div class="single-testimonial item d-flex flex-row">
								<div class="thumb">
									<img class="img-fluid" src="<?php echo get_bloginfo('template_directory'); ?>/img/user2.png" alt="">
								</div>
								<div class="desc">
									<p>
										Accessories Here you can find the best computer accessory for your laptop, monitor, printer, scanner, speaker, projector, hardware.
									</p>
									<h4 mt-30>Mark Alviro Wiens</h4>
									<p>CEO at Google</p>
								</div>
							</div>								
						</div>					
					</div>
				</div>	
			</section> -->
			<!-- End testimonial Area -->
			

			

<?php get_footer(); ?>
			
			