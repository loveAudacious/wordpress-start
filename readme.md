#Guide to setting up your dev environment
1. Download MAMP
2. Follow this guide (but skip step 9 onwards): https://skillcrush.com/2015/04/14/install-wordpress-mac/
3. Instead, git clone into the `sites/<your folder name>` folder

#Key things
- Bootstrap template from: https://colorlib.com/wp/template/creative-agency/
- As you can see, this is not build for wordpress usage. Not too sure if that affects our ability to install and setup a wordpress plugin easily

If you are reading this, thanks for contributing. Whooo